

import matplotlib as mpl

import time, array, random, copy, math, os, seaborn, itertools, operator

import numpy as np
import pandas as pd

pd.set_option('display.max_colwidth', -1)


import xarray as xr
import scipy.stats as stats
from scipy.misc import comb

from IPython.display import HTML, display_html, display
import matplotlib.pyplot as plt

seaborn.set_context('paper')
seaborn.set_style('ticks')

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('hatch', color='gray')
plt.rc('hatch', linewidth=0.5)

from matplotlib.ticker import StrMethodFormatter

def plot_assemble_boxplot(data, row_coords, col_coords, items_coords,
                          show_fig=True,
                          palette=None,
                          figsize=(11, 11),
                          sharey=True,
                          col_title_formatter='{}',
                          row_title_formatter='{}',
                          y_axis_formatter=None,
                          y_label_offset=None,
                          subplot_adjust=None, kvargs_boxplot={'fliersize':3}):
    'Returns a figure with the boxplots.'
    num_rows = len(data[row_coords])
    num_cols = len(data[col_coords])

    fig, axs = plt.subplots(num_rows, num_cols, figsize=figsize)

    if not palette:
        palette = seaborn.color_palette('Set1', len(data[items_coords]))

    for row, (row_name, row_data) in enumerate(data.groupby(row_coords,
                                                            squeeze=True)):
        mini, maxi = np.inf, -np.inf

        for col, (col_name, col_data) in enumerate(row_data.groupby(col_coords,
                                                                    squeeze=True)):
            box_data = col_data.to_pandas().squeeze()

            if sharey:
                mini = min(box_data.min().min(), mini)
                maxi = max(box_data.max().max(), maxi)

            if num_rows == 1:
                ax = axs[col]
            elif num_cols == 1:
                ax = axs[row]
            else:
                ax= axs[row, col]

            if col == 0:
                ax.spines['right'].set_color('none')
            else:
                ax.spines['left'].set_color('none')
                if not col == num_cols - 1:
                    ax.spines['right'].set_color('none')

            g = seaborn.boxplot(data=box_data, ax=ax, palette=palette,
                                saturation=0.74, width=0.38, notch=False,
                            linewidth=0.92, **kvargs_boxplot)

            if y_axis_formatter:
                ax.yaxis.set_major_formatter(y_axis_formatter)
                # ax.yaxis.set_minor_formatter(y_axis_formatter)

            if row == num_rows - 1:
                g.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)
                g.set_xlabel('')
            else:
                g.set_xticklabels([])
                g.set_xlabel('')

            if col == 0:
                g.set_ylabel(row_title_formatter.format(row_name))
                if y_label_offset:
                    g.yaxis.set_label_coords(y_label_offset, 0.5)
            else:
                if sharey:
                    ax.set_yticklabels([])

            # if col > 0:
            #    plt.setp(g.get_yticklabels(), visible=False)

            if row == 0:
                g.set_title(col_title_formatter.format(col_name))

            ax.grid(axis='both')

        if sharey:
            spread = (maxi - mini) * 0.1
            for col in range(num_cols):
                if num_rows == 1:
                    ax = axs[col]
                elif num_cols == 1:
                    ax = axs[row]
                else:
                    ax= axs[row, col]
                ax.set_ylim(mini - spread, maxi + spread)

    fig.tight_layout()

    if subplot_adjust:
        fig.subplots_adjust(**subplot_adjust)

    if not show_fig:
        plt.close(fig)

    return fig

# def plot_assemble_plot(data, row_coords, col_coords, items_coords,
#                           show_fig=True,
#                           palette=None,
#                           figsize=(11, 11),
#                           sharey=True,
#                           col_title_formatter='{}',
#                           row_title_formatter='{}',
#                           y_axis_formatter=None,
#                           y_label_offset=None,
#                           subplot_adjust=None):
#     'Returns a figure with the boxplots.'
#     num_rows = len(data[row_coords])
#     num_cols = len(data[col_coords])
#
#     fig, axs = plt.subplots(num_rows, num_cols, figsize=figsize)
#
#     if not palette:
#         palette = seaborn.color_palette('Set2', len(data[items_coords]))
#
#     for row, (row_name, row_data) in enumerate(data.groupby(row_coords,
#                                                             squeeze=True)):
#         mini, maxi = np.inf, -np.inf
#
#         for col, (col_name, col_data) in enumerate(row_data.groupby(col_coords,
#                                                                     squeeze=True)):
#             box_data = col_data.to_pandas().squeeze()
#
#             if sharey:
#                 mini = min(box_data.min().min(), mini)
#                 maxi = max(box_data.max().max(), maxi)
#
#             if num_rows == 1:
#                 ax = axs[col]
#             elif num_cols == 1:
#                 ax = axs[row]
#             else:
#                 ax= axs[row, col]
#
#             if col == 0:
#                 ax.spines['right'].set_color('none')
#             else:
#                 ax.spines['left'].set_color('none')
#                 if not col == num_cols - 1:
#                     ax.spines['right'].set_color('none')
#
#             g = seaborn.boxplot(data=box_data, ax=ax, palette=palette,
#                                 saturation=0.74, width=0.38, notch=False,
#                                 linewidth=0.92, fliersize=3)
#
#             if y_axis_formatter:
#                 ax.yaxis.set_major_formatter(y_axis_formatter)
#                 ax.yaxis.set_minor_formatter(y_axis_formatter)
#
#             if row == num_rows - 1:
#                 g.set_xticklabels(ax.xaxis.get_majorticklabels(), rotation=90)
#                 g.set_xlabel('')
#             else:
#                 g.set_xticklabels([])
#                 g.set_xlabel('')
#
#             if col == 0:
#                 g.set_ylabel(row_title_formatter.format(row_name))
#                 if y_label_offset:
#                     g.yaxis.set_label_coords(y_label_offset, 0.5)
#             else:
#                 if sharey:
#                     ax.set_yticklabels([])
#
#             # if col > 0:
#             #    plt.setp(g.get_yticklabels(), visible=False)
#
#             if row == 0:
#                 g.set_title(col_title_formatter.format(col_name))
#
#             ax.grid(axis='both')
#
#         if sharey:
#             spread = (maxi - mini) * 0.1
#             for col in range(num_cols):
#                 if num_rows == 1:
#                     ax = axs[col]
#                 elif num_cols == 1:
#                     ax = axs[row]
#                 else:
#                     ax= axs[row, col]
#                 ax.set_ylim(mini - spread, maxi + spread)
#
#     fig.tight_layout()
#
#     if subplot_adjust:
#         fig.subplots_adjust(**subplot_adjust)
#
#     if not show_fig:
#         plt.close(fig)
#
#     return fig

def compute_stat_matrix(data, stat_func, alpha=0.05, use_sidak_correction=True):
    '''A function that applies `stat_func` to all combinations of columns in
    the `data` Dataframe.
    Returns a squared matrix with the p-values.'''
    p_values = pd.DataFrame(columns=data.columns, index=data.columns)
    for a, b in itertools.combinations(data.columns,2):
        p = 0
        try:
            s,p = stat_func(data[a], data[b])
        except ValueError:
            # print('Columns', a,b,'identicals')
            pass
        p_values[a].ix[b] = p
        p_values[b].ix[a] = p

    if use_sidak_correction:
        # Sidak correction of alpha
        alpha = 1 - (1-alpha)**(1/comb(len(p_values.columns), 2))

    # a True value implies non-homogeneous results
    binary_outcome = p_values.applymap(lambda value: np.NAN if np.isnan(value) else value <= alpha)
    return p_values, binary_outcome

def test_plain_summary(data, test_result, comparator=operator.gt):
    'Assumes minimization problems.'
    summarized_result = pd.DataFrame(columns=data.columns, index=data.columns)
    means = data.mean()
    for a,b in itertools.combinations(data.columns,2):
        if test_result[a].ix[b]:
            if comparator(means[a], means[b]):
                summarized_result[a].ix[b] = '+'
                summarized_result[b].ix[a] = '-'
            else:
                summarized_result[a].ix[b] = '-'
                summarized_result[b].ix[a] = '+'
        else:
            summarized_result[a].ix[b] = '~'
            summarized_result[b].ix[a] = '~'
    for a in data.columns:
        summarized_result[a].ix[a] = 'x'
    return summarized_result

# html export stuff

def test_summary_html(data, p_values, test_result, show_p_values=False,
                      comparator=operator.gt, formatter='{:5.5g}'):
    'Assumes minimization problems.'
    summarized_result = pd.DataFrame(columns=data.columns, index=data.columns)

    means = data.mean()
    for a, b in itertools.combinations(data.columns, 2):
        pc = formatter.format(p_values[a].ix[b]) if show_p_values else '+'
        nc = formatter.format(p_values[a].ix[b]) if show_p_values else '-'
        ec = formatter.format(p_values[a].ix[b]) if show_p_values else '~'

        if test_result[a].ix[b]:
            if comparator(means[a], means[b]):
                summarized_result[a].ix[b] = '<div class="win">' + pc + '</div>'
                summarized_result[b].ix[a] = '<div class="lose">' + nc + '</div>'
            else:
                summarized_result[a].ix[b] = '<div class="lose">' + nc + '</div>'
                summarized_result[b].ix[a] = '<div class="win">' + pc + '</div>'
        else:
            summarized_result[a].ix[b] = '<div class="equ">' + ec + '</div>'
            summarized_result[b].ix[a] = '<div class="equ">' + ec + '</div>'
    for a in data.columns:
        summarized_result[a].ix[a] = '<div class="null">&nbsp;</div>'
    return summarized_result

def export_tests_summary_html(data, row_coords, col_coords, items_coords,
                      stat_func=stats.mannwhitneyu,
                      show_p_values=True,
                      comparator=operator.gt,
                      col_title_formatter='{}',
                      row_title_formatter='{}',):
    num_rows = len(data[row_coords])
    num_cols = len(data[col_coords])

    for row, (row_name, row_data) in enumerate(data.groupby(row_coords,
                                                            squeeze=True)):
        for col, (col_name, col_data) in enumerate(row_data.groupby(col_coords,
                                                                    squeeze=True)):
            col_df = col_data.to_pandas().squeeze()

            p_values, boolean_res = compute_stat_matrix(col_df, stat_func=stat_func)

            display(HTML('<h2>'+ col_title_formatter.format(col_name) + '; ' + row_title_formatter.format(row_name) + '.</h2>'))
            display(HTML(test_summary_html(col_df, p_values, boolean_res, show_p_values=show_p_values, comparator=comparator).to_html(escape=False)))

def html_notebook_initialization():
    return HTML('''<style>
        .win { text-align: center; background-color: LightGreen; }
        .lose { text-align: center; background-color: LightCoral; }
        .equ { text-align: center; background-color: LightBlue; }
        .null { text-align: center; background-color: LightGray; }
        </style>''')

## latex stat

def test_summary_latex(data, p_values, test_result, show_p_values=False,
                       comparator=operator.gt, formatter='{:2.3g}'):
    'Assumes minimization problems.'
    summarized_result = pd.DataFrame(columns=data.columns, index=data.columns)
    means = data.mean()
    for a, b in itertools.combinations(data.columns, 2):
        pc = formatter.format(p_values[a].ix[b]) if show_p_values else '+'
        nc = formatter.format(p_values[a].ix[b]) if show_p_values else '-'
        ec = formatter.format(p_values[a].ix[b]) if show_p_values else '\sim'

        if test_result[a].ix[b]:
            if comparator(means[a], means[b]):
                summarized_result[a].ix[b] = '\\positive{'+pc+'}'
                summarized_result[b].ix[a] = '\\negative{'+nc+'}'
            else:
                summarized_result[a].ix[b] = '\\negative{'+nc+'}'
                summarized_result[b].ix[a] = '\\positive{'+pc+'}'
        else:
            summarized_result[a].ix[b] = '\\equal{'+ec+'}'
            summarized_result[b].ix[a] = '\\equal{'+ec+'}'
    for a in data.columns:
        summarized_result[a].ix[a] = '\\diag'
    return summarized_result

def export_summary(summary, row_name='', col_name='',
                include_first_column=True, show_header=True,
                rotate_col_names=True,
                repeat_col_names=False, col_title_formatter='{}', row_title_formatter='{}', show_col_name=True):
    orig_cols = summary.columns

    if rotate_col_names:
        header_formatter = '\\rotatebox{{90}}{{{0}}}'
    else:
        header_formatter = '{{{0}}}'

    cols = [header_formatter.format(col) for col in orig_cols]
    # column_format='C{\\wid}@{}' *len(summary.columns)
    column_format='C{\\wid}' *len(summary.columns)


    if include_first_column:
        column_format='@{}cr' + column_format
        cols[0] = ' & ' + cols[0]
        rows = [' & '+val for val in summary.index.values]
        rows[0] = '\\multirow{{{0}}}{{*}}{{\\rotatebox[origin=c]{{90}}{{\\textbf{{{1}}}}}}}'.format(len(summary.columns), row_title_formatter.format(row_name)) + rows[0]
        summary['Formatted'] = rows
        # print(summary.Formatted)
        summary.set_index(['Formatted'], inplace=True)


    if show_col_name:
        if include_first_column:
            cols[0] = ' & ' + cols[0]

        cols[0] = '\\multicolumn{{{0}}}{{c}}{{\\textbf{{{1}}}}}\\\\'.format(len(summary.columns), col_title_formatter.format(col_name)) + cols[0]
        if include_first_column:
            cols[0] = ' & ' + cols[0]


    summary.index.name = ''
    summary.columns = cols

    fmtr = lambda x: str(x)
    res = summary.to_latex(index=include_first_column, index_names=False, header=show_header, escape=False, column_format=column_format, col_space=3, formatters=[fmtr] * len(summary.columns))

    summary.columns = orig_cols

    return res


def latex_render_one_test(cell_data, row_name, col_name, stat_func, alpha,
                          use_sidak_correction, comparator, show_p_values, show_header,
                          show_first_column, rotate_col_names,
                          repeat_col_names, col_title_formatter, row_title_formatter, show_col_name):

    df = cell_data.squeeze().to_pandas()

    p_values, test_result = compute_stat_matrix(df,
                                                stat_func=stat_func,
                                                alpha=alpha,
                                                use_sidak_correction=use_sidak_correction)

    summary = test_summary_latex(df, p_values, test_result,
                                 comparator=comparator,
                                 show_p_values=show_p_values)

    return export_summary(summary, row_name=row_name, col_name=col_name,
                          include_first_column=show_first_column,
                          show_header=show_header,
                          rotate_col_names=rotate_col_names,
                          repeat_col_names=repeat_col_names,
                          col_title_formatter=col_title_formatter,
                          row_title_formatter=row_title_formatter,
                          show_col_name=show_col_name)

def _export_by_cols(row_data, row_name, col_coords, stat_func, alpha,
                    use_sidak_correction, comparator,
                    show_p_values, rotate_col_names, repeat_col_names, col_title_formatter, row_title_formatter, show_header):
    latex_render = ''

    if len(row_data[col_coords].shape) == 0: # only one element?
        latex_render += latex_render_one_test(row_data, row_name, str(row_data[col_coords].values), stat_func,
                                              alpha, use_sidak_correction, comparator,
                                              show_p_values=show_p_values,
                                              show_first_column=True,
                                              show_header=show_header,
                                              rotate_col_names=rotate_col_names,
                                              repeat_col_names=repeat_col_names,
                                              col_title_formatter=col_title_formatter, row_title_formatter=row_title_formatter,
                                              show_col_name=False)
    else:
        for col, (col_name, col_data) in enumerate(row_data.groupby(col_coords, squeeze=True)):

            if col > 0:
                latex_render += '\n & % new cell\n\n'

            latex_render += latex_render_one_test(col_data, row_name, col_name, stat_func,
                                                  alpha, use_sidak_correction, comparator,
                                                  show_p_values = show_p_values,
                                                  show_first_column = col==0,
                                                  show_header = show_header,
                                                  rotate_col_names=rotate_col_names,
                                                  repeat_col_names=repeat_col_names,
                                                  col_title_formatter=col_title_formatter,
                                                  row_title_formatter=row_title_formatter,
                                                  show_col_name=True)
    return latex_render

def stat_tests_latex(data, row_coords, col_coords, items_coords,
                     stat_func=stats.mannwhitneyu,
                     alpha=0.05,
                     use_sidak_correction=True,
                     show_p_values=False,
                     rotate_col_names = True,
                     repeat_col_names = False,
                     comparator=operator.gt,
                     col_title_formatter='{}',
                     row_title_formatter='{}'):
    if len(data[row_coords].shape) == 0: # only one element?
        num_rows = 1
    else:
        num_rows = len(data[row_coords])

    if len(data[col_coords].shape) == 0: # only one element?
        num_cols = 1
    else:
        num_cols = len(data[col_coords])

    latex_render = '\\begin{tabular}{@{}c'+ '@{}c'* (num_cols-1) +'@{}}\n'

    if num_rows == 1:
        # only one row
        latex_render += _export_by_cols(data, str(data[row_coords].values),
                                        col_coords, stat_func,
                                        alpha, use_sidak_correction,
                                        comparator, show_p_values,
                                        rotate_col_names, repeat_col_names,
                                        col_title_formatter, row_title_formatter,
                                        True)
        latex_render += '\n\\\\ % new row\n\n'
    else:
        for row, (row_name, row_data) in enumerate(data.groupby(row_coords, squeeze=True)):
            latex_render += _export_by_cols(row_data, row_name, col_coords, stat_func,
                                                  alpha, use_sidak_correction,
                                                  comparator, show_p_values,
                                                  rotate_col_names,
                                                  repeat_col_names,
                                                  col_title_formatter, row_title_formatter,
                                                  row==0)

            latex_render += '\n\\\\ % new row\n\n'

    latex_render += '\\end{tabular}\n'

    return latex_render


# ranking plot stuff

def count_pluses(series):
    c = 0
    for item in series:
        if item == '+':
            c +=1
    return c

# def compute_rankings(data, x_axis_name, y_axis_name,
#                      comparator=operator.gt,
#                      stat_func=stats.mannwhitneyu,
#                      alpha=0.05,
#                      use_sidak_correction=True):
#     res = pd.DataFrame(columns=list(data[x_axis_name].values),
#                        index=list(data[y_axis_name].values))
#
#     for row, (row_name, row_data) in enumerate(data.groupby(y_axis_name, squeeze=True)):
#         return row_data
#         counter = pd.Series(index=list(data[y_axis_name].values),
#                             data=[0]*len(list(data[y_axis_name].values)))
#
#         for col, (col_name, col_data) in enumerate(row_data.groupby(x_axis_name, squeeze=True)):
#             col_df = col_data.to_pandas().squeeze()
#             _, outcome = compute_stat_matrix(col_df, stat_func, alpha, use_sidak_correction)
#             summary = test_plain_summary(col_df, outcome, comparator=comparator)
#             return summary, counter
#             counter = counter.add(summary.apply(count_pluses, axis=1))
#             return counter
#         res.ix[row] = counter/(len(res.columns))
#
#     return res

def compute_ranking(data, groupby_name, grouped_name, targets_name,
                     comparator = operator.gt,
                     stat_func = stats.mannwhitneyu,
                     alpha=0.05,
                     use_sidak_correction=True):

    res = pd.DataFrame(columns=data[groupby_name],
                       index=data[targets_name])

    for i, (gby_name, gby) in enumerate(data.groupby(groupby_name, squeeze=True)):
        targets = data[targets_name]
        counter = pd.Series(index=targets, data=[0]*len(targets))
        for j, (gpedby_name, gpedby) in enumerate(gby.groupby(grouped_name, squeeze=True)):
            df = gpedby.to_pandas().squeeze()
            _, outcome = compute_stat_matrix(df, stat_func, alpha)
            summary = test_plain_summary(df, outcome, comparator=comparator)
            #print(gby_name, gpedby_name)
            #print(summary)
            counter = counter.add(summary.apply(count_pluses, axis=1))
        res[gby_name] = counter/ ((len(data[targets_name])-1) * len(data[grouped_name]))

    return res.transpose()

markers = ['o', 's', 'd', 'p', '8', '^', 'h', 'v', 'x', '+', '*', '1']
ls = ['-', '--', '-.', ':']

def ranking_plot(summarized_results,
                 title=None,
                 x_label=None, xtick_labels=None,
                 y_label='Average ranking',
                 ytick_labels=None,
                 show_legend=True,
                 palette=None,
                 show_fig=True,
                 figsize=(7, 4), ax=None, **kvargs):

    if not palette:
        palette = seaborn.color_palette('Set1', len(summarized_results.columns))

    # fix this
    fig = plt.figure(figsize=figsize)

    if not ax:
        ax = plt.subplot(111)


    plt.ylim(-0.10, np.max(np.max(summarized_results))+0.1)
    plt.xlim(-0.3, len(summarized_results)-0.7)

    plt.xticks(np.arange(0, len(summarized_results)), summarized_results.index.values)

    last = summarized_results.iloc[-1]

    for index, col in enumerate(summarized_results.columns):
        ax.plot(range(len(summarized_results)), summarized_results[col], color=palette[index],
                markerfacecolor='None', markeredgewidth=1,
                markeredgecolor=palette[index],
                marker=markers[index],
                ms=4,
                lw=1, #markerfacecolor='None',
                linestyle=ls[index % len(ls)],
                label=col, alpha=0.92, **kvargs)
        #ax.text(len(problem_instances)- 0.9, last[col]-0.1, col, fontsize=11, color=pal[index])

    if show_legend:
        ax.legend(loc='center left', bbox_to_anchor=(1,0.5), frameon=True, scatterpoints=1)

    ax.grid(axis='both')

    if xtick_labels is not None:
        ax.set_xticklabels(xtick_labels)
    if ytick_labels is not None:
        ax.set_yticklabels(ytick_labels)

    if x_label:
        plt.xlabel(x_label)

    plt.ylabel(y_label)

    if title:
        plt.title(title)

    if not show_fig:
        fig.close()

    return fig
